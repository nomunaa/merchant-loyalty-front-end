import React, { PureComponent } from "react";
import {
  Col,
  Container,
  Row,
  Card,
  CardBody,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
  CardTitle,
} from "reactstrap";
import ReactDOM from "react-dom";
import { reduxForm } from "redux-form";
import classnames from "classnames";
import ExampleCard from "./components/ExampleCard";
import Tab2 from "./components/Tab2";
import "../../scss/component/navbar.scss";
import { withTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { NotificationContainer } from "react-notifications";
import axios from "axios";

class BurtgelErh extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: "1",
      company_id: "5bd95078b92b980bb3f1fb91",
      hrs: [],
      always: [],
      ceos: [],
    };
    this.handleChange = this.handleChange.bind(this);
    this.onSearch = this.onSearch.bind(this);
    // this.onSubmit = this.onSubmit.bind(this);
  }
  componentDidMount() {
    axios
      .get("http://10.10.10.123:4000/admin/getHr/" + this.state.company_id)
      .then((response) => {
        this.setState({ hrs: response.data, always: response.data });
        console.log(this.state.company_id, "company");
      })
      .catch(function (error) {
        console.log(error);
      });
    axios
      .get("http://10.10.10.123:4000/admin/getDirector/" + this.state.company_id)
      .then((response) => {
        this.setState({ ceos: response.data });
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  onSearch() {
    axios
      .get("http://10.10.10.123:4000/admin/getHr/" + this.state.company_id)
      .then((response) => {
        this.setState({ hrs: response.data });
        console.log(this.state.company_id, "company");
      })
      .catch(function (error) {
        console.log(error);
      });
    axios
      .get("http://10.10.10.123:4000/admin/getDirector/" + this.state.company_id)
      .then((response) => {
        this.setState({ ceos: response.data });
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  handleChange(e) {
    this.setState({ company_id: e.target.value });
  }

  toggle = (tab) => {
    const { activeTab } = this.state;
    if (activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
    console.log(this.state.hrs, "hrs");
    console.log(this.state.ceos, "ceos");
    console.log(this.state.company_id, "company_id");
  };

  render() {
    const { activeTab } = this.state;
    let { hrs } = this.state;
    let { ceos } = this.state;
    let { company_id } = this.state;
    return (
      <div className="content">
        <Container style={{ marginLeft: "0px" }}>
          <Row>
            <Col>
              <button className="btn">
                <Link
                  to={"/pages/adminBurtgel/add/"}
                  style={{ color: "white" }}
                >
                  + Бүртгэлийн эрх үүсгэх
                </Link>
              </button>
            </Col>
            <NotificationContainer />
          </Row>
          <Row>
            <Card>
              <CardBody style={{ height: "10%", width: "100%" }}>
                <Col>
                  <CardTitle className="card__title">
                    Бүртгэлийн эрх харах
                  </CardTitle>
                  <hr />
                  {/* <div>
                    <p
                      style={{
                        fontFamily: "Montserrat",
                        fontSize: "22px",
                        color: "#057AC4",
                        fontWeight: 400,
                      }}
                    >
                      Бүртгэлийн эрх харах
                    </p>
                    <hr />
                  </div> */}
                </Col>
                {/* <form onSubmit={this.onSearch}> */}
                <Row style={{ marginLeft: "10px", marginTop: "0%" }}>
                  <Col md={2}>
                    <div>
                      <span
                        className="form__form-group-label"
                        style={{ marginTop: "10px" }}
                      >
                        Байгууллага
                      </span>
                      <select
                        value={this.state.company_id}
                        onChange={this.handleChange}
                        className="select-index"
                      >
                        <option value="5bd95078b92b980bb3f1fb91">
                          Юнайтэд аллианс
                        </option>

                      </select>
                    </div>
                  </Col>
                  <Col>
                    <button
                      className="btn"
                      type="submit"
                      onClick={this.onSearch}
                      style={{ marginTop: "34px" }}
                    >
                      Шалгах
                    </button>
                  </Col>
                </Row>
                {/* </form> */}
              </CardBody>
            </Card>
          </Row>
          <Row>
            <Col md={5}>
              <div className="tabs tabs--justify">
                <div className=" tabs tabs__wrap">
                  <Nav tabs>
                    <NavItem>
                      <NavLink
                        style={{
                          fontFamily: "Montserrat",
                          fontSize: "20px",
                          color: "#057AC4",
                          fontWeight: 400,
                        }}
                        className={classnames({ active: activeTab === "1" })}
                        onClick={() => {
                          this.toggle("1");
                        }}
                      >
                        Бүртгэх эрх
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        style={{
                          fontFamily: "Montserrat",
                          fontSize: "20px",
                          color: "#057AC4",
                          fontWeight: 4000,
                        }}
                        className={classnames({ active: activeTab === "2" })}
                        onClick={() => {
                          this.toggle("2");
                        }}
                      >
                        Баталгаажуулах эрх
                      </NavLink>
                    </NavItem>
                  </Nav>
                </div>
              </div>
            </Col>
          </Row>
          <Row>
            <Col lg={12}>
              <TabContent activeTab={activeTab}>
                <TabPane tabId="1">
                  <ExampleCard hrs={hrs} company_id={company_id} />
                </TabPane>
                <TabPane tabId="2">
                  <Tab2 ceos={ceos} company_id={company_id} />
                </TabPane>
              </TabContent>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
export default reduxForm({
  form: "vertical_form", // a unique identifier for this form
})(withTranslation("common")(BurtgelErh));
