import React, { PureComponent } from "react";
import { Card, Col } from "reactstrap";
import axios from "axios";
import moment from "moment";
import CardBody from "reactstrap/lib/CardBody";
import CardTitle from "reactstrap/lib/CardTitle";
import { NotificationManager } from "react-notifications";

class Zasah extends PureComponent {
  constructor(props) {
    super(props);

    this.onChangeUsername = this.onChangeUsername.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.onSignType = this.onSignType.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      username: "",
      email: "",
      merchant_id: "",
      role: "Бүртгэх эрх",
      updated_at: "",
    };
  }

  componentDidMount() {
    axios
      .get("http://10.10.10.123:4000/admin/" + this.props.match.params.id)
      .then((response) => {
        this.setState({
          username: response.data.username,
          email: response.data.email,
          merchant_id: response.data.merchant_id,
          role: response.data.role,
          updated_at: response.data.updated_at,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }
  onChangeUsername(e) {
    this.setState({
      username: e.target.value,
    });
  }
  toggle() {
    this.setState((prevState) => ({ modal: !prevState.modal }));
  }

  onChangeEmail(e) {
    this.setState({
      email: e.target.value,
    });
  }
  handleChange(e) {
    this.setState({ merchant_id: e.target.value });
  }
  onSignType(e) {
    this.setState({ role: e.target.value });
  }

  onSubmit(e) {
    e.preventDefault();

    const obj = {
      username: this.state.username,
      email: this.state.email,
      merchant_id: this.state.merchant_id,
      role: this.state.role,
      updated_at: moment().format(),
    };

    axios
      .post(
        "http://10.10.10.123:4000/admin/update/" + this.props.match.params.id,
        obj
      )
      .then((res) => {
        this.props.history.push("/pages/burtgelerh");
        console.log(res.data);
        NotificationManager.success(res.data.message, "", 5000);
      });
  }

  render() {
    return (
      <Col md={12} lg={12}>
        <Card className="card-zasah">
          <CardBody>
            <CardTitle className="card__title">Бүртгэл Засах</CardTitle>
            <hr className="line" />
            <div style={{ marginTop: 20 }}>
              <form className="form form--horizontal" onSubmit={this.onSubmit}>
                <div className="form__form-group">
                  <span className="form__form-group-label">
                    Байгууллагын нэр
                  </span>
                  <div className="form__form-group-field">
                    <select
                      value={this.state.merchant_id}
                      onChange={this.handleChange}
                      disabled
                    >
                      <option value="5bcd43443fe112b9d6160d3d">Юнител</option>
                      <option value="5cd404ce5f627d42a5786602">МТ</option>
                      <option value="5d19afa55f627d26cfca48d7">Монос</option>
                      <option value="5c01f71b6dc84937dd321d1b">
                        Тү Лэ Жүр
                      </option>
                      <option value="5c10bc165f627d320c0969c6">Юнивишн</option>
                      <option value="607412575f627d089681fb74">
                        Гэр интернэт
                      </option>
                      <option value="5d142d215f627d62c382f155">Ддэш</option>
                      <option value="5c009abf5f627d2b147a6ad1">
                        Кафе Бэнэ
                      </option>
                    </select>
                  </div>
                </div>
                <div className="form__form-group">
                  <span className="form__form-group-label">Нэвтрэх нэр</span>
                  <div className="form__form-group-field">
                    <input
                      name="username"
                      component="input"
                      type="text"
                      placeholder="Нэвтрэх нэр"
                      value={this.state.username}
                      onChange={this.onChangeUsername}
                      disabled
                    />
                  </div>
                </div>
                <div className="form__form-group">
                  <span className="form__form-group-label">И-мэйл хаяг</span>
                  <div className="form__form-group-field">
                    <input
                      name="email"
                      disabled
                      component="input"
                      type="email"
                      placeholder="И-мэйл хаяг"
                      value={this.state.email}
                      onChange={this.onChangeEmail}
                    />
                  </div>
                </div>
                <div className="form__form-group">
                  <span className="form__form-group-label">
                    Бүртгэлийн эрх сонгох
                  </span>
                  <div className="form__form-group-field">
                    <select value={this.state.role} onChange={this.onSignType}>
                      <option value="hr">Бүртгэх эрх</option>
                      <option value="director">Баталгаажуулах эрх</option>
                    </select>
                  </div>
                </div>
                <div className="form-group">
                  <input type="submit" value="Хадгалах" className="uusgeh" />
                </div>
              </form>
            </div>
          </CardBody>
        </Card>
      </Col>
    );
  }
}
export default Zasah;
