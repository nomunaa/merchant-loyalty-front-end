import React, { PureComponent } from "react";
import { Card, Col, Row, Table, Button } from "reactstrap";
import axios from "axios";
import Todos from "../components/table";

class Tab1 extends PureComponent {
  constructor(props) {
    super(props);

    this.harii = this.harii.bind(this);
  }

  harii() {
    console.log(this.props.hrs, "orj irsen todos");
    console.log(this.props.company_id, "orj irsen company_id");
  }
  todoList() {
    return this.props.hrs.map(function (currentHRs, i) {
      return <Todos hrs={currentHRs} key={i} />;
    });
  }

  render() {
    return (
      <Card>
        <Row>
          <Col>
            {/* <Button onClick={this.harii}></Button> */}
            <Card body style={{ height: "auto", width: "100%" }}>
              <div style={{ marginLeft: "3%" }}>
                <Table
                  className="table--bordered table--head-accent"
                  responsive
                  hover
                >
                  <thead>
                    <tr>
                      <th style={{ width: "300px" }}>Бүртгэх эрх</th>
                      <th style={{ width: "300px" }}>Имэйл хаяг</th>

                      <th style={{ width: "300px" }}>Эрх үүсгэсэн огноо</th>
                      <th style={{ width: "100px" }}></th>
                      <th style={{ width: "100px" }}></th>
                    </tr>
                  </thead>
                  <tbody>{this.todoList()}</tbody>
                </Table>
              </div>
            </Card>
          </Col>
        </Row>
      </Card>
    );
  }
}

export default Tab1;
