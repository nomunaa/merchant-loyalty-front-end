import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import axios from "axios";
import {
  NotificationManager,
  NotificationContainer,
} from "react-notifications";
import moment from "moment";
import { Badge, Button, ButtonToolbar, Modal } from "reactstrap";
class verifyTab extends Component {
  constructor(props) {
    super(props);
    this.deleteStudent = this.deleteStudent.bind(this);
    this.unBlock = this.unBlock.bind(this);
    this.state = {
      modal: false,
    };
    this.toggle = this.toggle.bind(this);
  }
  toggle() {
    this.setState((prevState) => ({ modal: !prevState.modal }));
  }
  deleteStudent() {
    axios
      .delete("http://10.10.10.123:4000/admin/delete/" + this.props.todo._id)
      .then((res) => {
        console.log(res.data);
        NotificationManager.success(res.data.message, "", 5000);
        window.location.reload();
      })
      .catch((error) => {
        console.log(error);
      });
    this.toggle();
  }
  unBlock(e) {
    e.preventDefault();
    axios
      .post("http://10.10.10.123:4000/admin/unblock/" + this.props.todo._id)
      .then((res) => {
        this.props.history.push("/pages/burtgelerh");
        console.log(res.data);
        NotificationManager.success(res.data.message, "", 5000);
      });
  }

  render() {
    const { modal } = this.state;
    return (
      <tr>
        <td>{this.props.todo.username}</td>
        <td>{this.props.todo.email}</td>
        <td>{moment(this.props.todo.created_at).format("L")}</td>
        {/* <td>
              <Link to={"/edit/" + props.todo._id}>Edit</Link>
            </td> */}
        <td style={{ width: "100px" }}>
          <Badge
            color="dark"
            onClick={this.unBlock}
            to={"/pages/adminBurtgel/edit/" + this.props.todo._id}
            style={{ color: "white" }}
          >
            Unblock
          </Badge>
          <NotificationContainer />
        </td>
        <td style={{ width: "100px" }}>
          <Badge color="warning">
            <Link
              to={"/pages/adminBurtgel/edit/" + this.props.todo._id}
              style={{ color: "white" }}
            >
              Засах
            </Link>
          </Badge>
          <NotificationContainer />
        </td>
        <td style={{ width: "100px" }}>
          <Badge color="danger" onClick={this.toggle}>
            Устгах
          </Badge>
          <Modal isOpen={modal} toggle={this.toggle}>
            <div className="modal__header"></div>
            <div className="modal__body">
              {" "}
              Ажилтан {this.props.todo.username} -г устгах уу?
            </div>
            <ButtonToolbar className="modal__footer">
              <Button className="modal_ok" onClick={this.deleteStudent}>
                Тийм
              </Button>{" "}
              <Button className="modal_cancel" onClick={this.toggle}>
                Үгүй
              </Button>{" "}
            </ButtonToolbar>
          </Modal>
        </td>
      </tr>
    );
  }
}
export default withRouter(verifyTab);
