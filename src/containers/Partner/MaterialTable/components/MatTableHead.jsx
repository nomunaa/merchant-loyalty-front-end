import React, { PureComponent } from "react";
import { connect } from "react-redux";
import TableHead from "@material-ui/core/TableHead";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Checkbox from "@material-ui/core/Checkbox";
import TableSortLabel from "@material-ui/core/TableSortLabel";

const rows = [
  {
    id: "last_name",
    disablePadding: true,
    label: "Овог",
  },
  {
    id: "first_name",
    disablePadding: false,
    label: "Нэр",
  },
  {
    id: "registration_number",
    disablePadding: false,
    label: "Регистрийн дугаар",
  },
  {
    id: "phone_number",
    disablePadding: false,
    label: "Утасны дугаар",
  },
  {
    id: "department",
    disablePadding: false,
    label: "Газар хэлтэс",
  },
  {
    id: "position",
    disablePadding: false,
    label: "Албан тушаал",
  },
  {
    id: "home_address",
    disablePadding: false,
    label: "Гэрийн хаяг",
  },
  {
    id: "segment",
    disablePadding: false,
    label: "Сегмент",
  },
  {
    id: "work_started_at",
    disablePadding: false,
    label: "Ажилд орсон огноо",
  },
];

class MatTableHead extends PureComponent {
  createSortHandler = (property) => (event) => {
    const { onRequestSort } = this.props;
    onRequestSort(event, property);
  };

  render() {
    const {
      onSelectAllClick,
      order,
      orderBy,
      numSelected,
      rowCount,
      rtl,
    } = this.props;

    return (
      <TableHead>
        <TableRow>
          <TableCell padding="checkbox">
            <Checkbox
              className={`material-table__checkbox ${
                numSelected === rowCount && "material-table__checkbox--checked"
              }`}
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={numSelected === rowCount}
              onChange={onSelectAllClick}
            />
          </TableCell>
          {rows.map(
            (row) => (
              <TableCell
                className="material-table__cell material-table__cell--sort material-table__cell-right"
                key={row.id}
                align={rtl.direction === "rtl" ? "right" : "left"}
                padding={row.disablePadding ? "none" : "default"}
                sortDirection={orderBy === row.id ? order : false}
              >
                <TableSortLabel
                  active={orderBy === row.id}
                  direction={order}
                  onClick={this.createSortHandler(row.id)}
                  className="material-table__sort-label"
                  dir="ltr"
                >
                  {row.label}
                </TableSortLabel>
              </TableCell>
            ),
            this
          )}
        </TableRow>
      </TableHead>
    );
  }
}

export default connect((state) => ({
  rtl: state.rtl,
}))(MatTableHead);
