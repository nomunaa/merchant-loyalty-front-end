import React, { PureComponent } from "react";
import {
  Col,
  Container,
  Row,
  Card,
  CardBody,
  CardTitle,
  Modal,
  ButtonToolbar,
  Badge
} from "reactstrap";

import axios from "axios";
import Todos from "../../Partner/components/deleteEmployee";
import { DataGrid, GridToolbar } from "@material-ui/data-grid";
import { Button } from "antd";
import "antd/dist/antd.css";
import {
  NotificationManager,
  NotificationContainer,
} from "react-notifications";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
import moment from "moment";

const columns = [
  // {
  //   field: "field_status",
  //   headerName: "Төлөв",
  //   width: 100,
  //   color: "blue",
  //   renderCell: (params) => {
  //     if (params.row.field_status == "Шинэ") {
  //       return (
  //         <Badge style={{ backgroundColor: "green" }}>
  //           {params.row.field_status}
  //         </Badge>
  //       );
  //     } else if (params.row.field_status == "Баталгаажсан") {
  //       return (
  //         <Badge style={{ backgroundColor: "green" }}>
  //           {params.row.field_status}
  //         </Badge>
  //       );
  //     } else if (params.row.field_status == "Хүлээгдэж буй") {
  //       return (
  //         <Badge style={{ backgroundColor: "blue" }}>
  //           {params.row.field_status}
  //         </Badge>
  //       );
  //     }
  //   },
  // },


  {
    field: "last_name",
    headerName: "Овог",
    width: 150,
    color: "blue",
    // renderCell: renderCellExpand
  },
  { field: "first_name", headerName: "Нэр", width: 170 },
  { field: "registration_number", headerName: "Регистрийн дугаар", width: 150 },
  { field: "email", headerName: "Имэйл", width: 190 },
  { field: "phone_number", headerName: "Утасны дугаар", width: 110 },
  {
    field: "ua_code", headerName: "UA Код", width: 240,
  },
  { field: "shop_name", headerName: "Дэлгүүрийн нэр", width: 180 },

  {
    field: "work_started_at",
    headerName: "Гэрээ хийсэн огноо",
    width: 120,
    type: "date",
    valueGetter: (params) => {
      let result = params.row.work_started_at;
      let wsa = moment(result).format("L");
      return wsa;
    },
  },
  {
    field: "employee_status",
    headerName: "Ажилтны статус",
    width: 200,
    color: "blue",
    renderCell: (params) => {
      if (params.row.employee_status == "Ажиллаж байгаа") {
        return (
          <Badge style={{ backgroundColor: "green" }}>
            {params.row.employee_status}
          </Badge>
        );
      } else if (params.row.employee_status == "Гэрээт") {
        return (
          <Badge style={{ backgroundColor: "yellow" }}>
            {params.row.employee_status}
          </Badge>
        );
      } else if (params.row.employee_status == "Хүүхэд асрах чөлөөтэй") {
        return (
          <Badge style={{ backgroundColor: "blue" }}>
            {params.row.employee_status}
          </Badge>
        );
      } else if (params.row.employee_status == "Чөлөөтэй") {
        return (
          <Badge style={{ backgroundColor: "pink" }}>
            {params.row.employee_status}
          </Badge>
        );
      }
    },
  },
  // {
  //   field: "emp_status_change_date",
  //   headerName: "Статус солигдсон огноо",
  //   width: 120,
  //   valueGetter: (params) => {
  //     let result = params.row.emp_status_change_date;
  //     let wsa = moment(result).format("L");

  //     return wsa;
  //   },
  // },
];
var rowsToKeep = [];

class Verified extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { todos: [], rn: "", phone: "", name: "", modal: false, employee_status: "" };
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangeRN = this.onChangeRN.bind(this);
    this.onChangePhone = this.onChangePhone.bind(this);
    this.onSearch = this.onSearch.bind(this);
    this.toggle = this.toggle.bind(this);
    this.onChangeEmpStatus = this.onChangeEmpStatus.bind(this);
    this.deleteStudent = this.deleteStudent.bind(this);
  }

  componentDidMount() {
    axios
      .get("http://10.10.10.123:4000/employee/verified_hr", {
        params: {
          name: this.state.name,
          phone: this.state.phone,
          rn: this.state.rn,
          employee_status: this.state.employee_status
        },
      })
      .then((response) => {
        this.setState({ todos: response.data });
        console.log(response.data, "todos");
      })
      .catch(function (error) {
        console.log(error);
      });
  }
  onSearch() {
    axios
      .get("http://10.10.10.123:4000/employee/verified_hr", {
        params: {
          name: this.state.name,
          phone: this.state.phone,
          rn: this.state.rn,
          employee_status: this.state.employee_status
        },
      })
      .then((response) => {
        this.setState({ todos: response.data });
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  toggle() {
    this.setState((prevState) => ({ modal: !prevState.modal }));
    console.log(this.props, "props");
  }

  reject(e) {
    e.preventDefault();

    axios
      .post("http://10.10.10.123:4000/employee/reject/" + rowsToKeep)
      .then((res) => {
        console.log(res.data);
        NotificationManager.success(res.data.message, "", 5000);
      });
  }
  onChangeName(e) {
    this.setState({ name: e.target.value });
    console.log(this.state.employees, "eniig harddss");
  }
  onChangeRN(e) {
    this.setState({ rn: e.target.value });
  }
  onChangePhone(e) {
    this.setState({ phone: e.target.value });
  }
  onChangeEmpStatus(e) {
    this.setState({ employee_status: e.target.value });
  }
  todoList() {
    return this.state.todos.map(function (currentTodo, i) {
      return <Todos todo={currentTodo} key={i} />;
    });
  }
  handleRowSelection = (e) => {
    rowsToKeep = e;;
    console.log("selected ids: " + rowsToKeep);
  };

  deleteStudent() {
    axios
      .post("http://10.10.10.123:4000/employee/apply_delete/", rowsToKeep)
      .then((res) => {
        console.log(res.data, "res.data");
        NotificationManager.success(res.data.message, "", 5000);
        window.location.reload();
      })
      .catch((error) => {
        NotificationManager.error(error.message, "", 5000);
        console.log(error);
      });
    this.toggle();
    // window.location.reload(false);
  }


  render() {
    const { modal } = this.state;


    return (
      <div className="content">
        <Container >
          <NotificationContainer />
          <Row>
            <Card>
              <CardBody style={{ height: "auto", width: "100%" }}>

                <Row style={{ marginLeft: "10px" }}>
                  {/* <form className="form" onSubmit={handleSubmit}> */}
                  <div className="form">
                    <Col>
                      <div className="form__form-group">
                        {/* <span className="form__form-group-label">Нэр</span> */}
                        <div className="form__form-group-field">
                          <input
                            name="name"
                            component="input"
                            type="text"
                            placeholder="Нэр"
                            value={this.state.name}
                            onChange={this.onChangeName}
                          />
                        </div>
                      </div>
                    </Col>
                    <Col>
                      <div className="form__form-group">
                        {/* <span className="form__form-group-label">
                          Регистрийн дугаар
                        </span> */}
                        <div className="form__form-group-field">
                          <input
                            name="registration_number"
                            component="input"
                            type="text"
                            placeholder="Регистрийн дугаар"
                            value={this.state.rn}
                            onChange={this.onChangeRN}
                          />
                        </div>
                      </div>
                    </Col>
                    <Col>
                      <div className="form__form-group">
                        {/* <span className="form__form-group-label">
                          Утасны дугаар{" "}
                        </span> */}
                        <div className="form__form-group-field">
                          <input
                            name="phone_number"
                            component="input"
                            type="text"
                            placeholder="Утасны дугаар"
                            value={this.state.phone}
                            onChange={this.onChangePhone}
                          />
                        </div>
                      </div>
                    </Col>
                    <Col>
                      <div className="form__form-group">
                        <div className="form__form-group-field">
                          <select
                            value={this.state.employee_status}
                            onChange={this.onChangeEmpStatus}
                            placeholder="Ажилтны статус"
                            // className="select-index"
                            style={{ height: "32px", borderColor: "#f4f7f8", boxShadow: "none", color: "gray" }}
                          >
                            <option value="" defaultValue >
                              Ажилтны статус
                            </option>
                            <option value='1'>Ажиллаж байгаа</option>
                            <option value="2">Хүүхэд асрах чөлөөтэй</option>
                            <option value="3">Чөлөөтэй</option>
                            <option value="4">Гэрээт</option>
                          </select>
                        </div>
                      </div>
                    </Col>
                    <Col>
                      <button
                        className="btn"
                        type="submit"
                        onClick={this.onSearch}

                      >
                        Хайх
                      </button></Col>
                  </div>
                  {/* </form> */}
                </Row>

                <Row>

                  <div style={{ height: "610px", width: "100%", margin: "10px 10px" }}>
                    <DataGrid
                      getRowId={(row) => row._id}
                      rows={this.state.todos}
                      columns={columns}
                      rowsPerPageOptions={[30, 50, 100]}
                      // pageSize={15}
                      checkboxSelection
                      components={{
                        Toolbar: GridToolbar
                      }}
                      disableColumnMenu
                      onSelectionModelChange={this.handleRowSelection}
                      onRowDoubleClick={(e) =>
                        this.props.history.push(
                          "/partner/apply_edit/" + e.id
                        )
                      }
                    />
                  </div>
                </Row>
                <Row style={{ marginLeft: "0%" }}>
                  <Col md={4}></Col><Col md={6}>
                    <Button
                      className="btn"
                      style={{ backgroundColor: "red", color: "white", width: "450px", height: "40px" }}
                      onClick={this.toggle}
                    >
                      Устгах хүсэлт илгээх
                    </Button>
                    <Modal isOpen={modal} toggle={this.toggle}>
                      <div className="modal__header"></div>
                      <div className="modal__body">
                        {" "}
                        Ажилтан-г устгах хүсэлт илгээх үү?
                      </div>
                      <ButtonToolbar className="modal__footer">
                        <Button
                          className="modal_ok"
                          onClick={this.deleteStudent}
                        >
                          Тийм
                        </Button>{" "}
                        <Button
                          className="modal_cancel"
                          onClick={this.toggle}
                        >
                          Үгүй
                        </Button>{" "}
                      </ButtonToolbar>
                    </Modal>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Row>

        </Container>
      </div>
    );
  }
}

export default Verified;
