import React, { Component } from "react";
import { withRouter } from "react-router-dom";
class District extends Component {
  render() {
    return (
      <option value={this.props.work_districts._id}>
        {this.props.work_districts.name}
      </option>
    );
  }
}
export default withRouter(District);
