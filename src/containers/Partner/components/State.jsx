import React, { Component } from "react";
import { withRouter } from "react-router-dom";
class State extends Component {
  render() {
    return (
      <option value={this.props.states.name}>{this.props.states.name}</option>
    );
  }
}
export default withRouter(State);
