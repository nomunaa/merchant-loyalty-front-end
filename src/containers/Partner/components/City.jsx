import React, { Component } from "react";
import { withRouter } from "react-router-dom";
class City extends Component {
  render() {
    return (
      <option value={this.props.cities._id}>{this.props.cities.name}</option>
    );
  }
}
export default withRouter(City);
